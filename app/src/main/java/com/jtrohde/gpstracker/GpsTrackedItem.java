package com.jtrohde.gpstracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Represents an object in terms of GPS coordinates.
 *
 * @author Justin Rohde
 */
public class GpsTrackedItem {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LatLong getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLong latLong) {
        this.latLong = latLong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String name;
    private String description;
    private LatLong latLong;

    /**
     * Constructor.
     *
     * @param id The unique ID of the object.
     * @param name The name of the object.
     * @param description A description of the object.
     * @param latLong The geographic coordinate of the object.
     */
    public GpsTrackedItem(int id, String name, String description, LatLong latLong) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.latLong = latLong;
    }

    /**
     * Convert JSON array into java.util.ArrayList.
     *
     * @param json The JSON array of GPS-tracked items.
     * @return The array list of objects.
     */
    public static ArrayList<GpsTrackedItem> jsonToArrayList(String json) {
        ArrayList<GpsTrackedItem> items = new ArrayList<>();
        try {
            JSONArray result = new JSONArray(json);
            for (int i = 0; i < result.length(); i++) {
                JSONObject obj = result.getJSONObject(i);

                int id = obj.getInt("id");
                String name = obj.getString("name");
                String description = obj.getString("description");
                LatLong latLong = new LatLong(
                        Double.parseDouble(obj.getString("latitude")),
                        Double.parseDouble(obj.getString("longitude"))
                );

                GpsTrackedItem item = new GpsTrackedItem(id, name, description, latLong);
                items.add(item);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return items;
    }

}
