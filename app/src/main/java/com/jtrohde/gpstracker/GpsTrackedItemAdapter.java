package com.jtrohde.gpstracker;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author Justin Rohde
 */
public class GpsTrackedItemAdapter extends ArrayAdapter<GpsTrackedItem> {

    static class ViewHolder {
        TextView nameTextView;
        TextView latitudeTextView;
        TextView longitudeTextView;
        TextView descriptionTextView;
    }

    public GpsTrackedItemAdapter(Context context, List<GpsTrackedItem> objects) {
        super(context, 0, objects);
    }

    /**
     * Remove the item at the specified position.
     *
     * @param position The position of the item to remove.
     */
    public void removeItemAt(int position) {
        remove(getItem(position));
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GpsTrackedItem item = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.gps_tracked_item, parent, false);

            ViewHolder holder = new ViewHolder();
            holder.nameTextView = (TextView) convertView.findViewById(R.id.name);
            holder.latitudeTextView = (TextView) convertView.findViewById(R.id.latitude);
            holder.longitudeTextView = (TextView) convertView.findViewById(R.id.longitude);
            holder.descriptionTextView = (TextView) convertView.findViewById(R.id.description);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        holder.nameTextView.setText(item.getName());
        holder.latitudeTextView.setText(String.valueOf(item.getLatLong().getLatitude()));
        holder.longitudeTextView.setText(String.valueOf(item.getLatLong().getLongitude()));
        holder.descriptionTextView.setText((item.getDescription()));

        return convertView;
    }
}
