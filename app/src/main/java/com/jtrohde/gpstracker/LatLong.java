package com.jtrohde.gpstracker;

/**
 * A geographic coordinate, expressed in terms of degrees latitude and degrees longitude.
 *
 * @author Justin Rohde
 */
public class LatLong {
    /**
     * Latitude in degrees.
     */
    private double latitude;

    /**
     * Longitude in degrees.
     */
    private double longitude;

    /**
     * Constructor.
     *
     * @param latitude The latitude to set, in decimal degrees.
     * @param longitude The longitude to set, in decimal degrees.
     */
    public LatLong(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
