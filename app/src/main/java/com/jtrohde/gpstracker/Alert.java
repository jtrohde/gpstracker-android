package com.jtrohde.gpstracker;

import android.app.AlertDialog;
import android.content.Context;

/**
 * @author Justin Rohde
 */
public class Alert {

    /**
     * Display a simple alert message with an "OK" button.
     *
     * @param context The context on which to display the alert.
     * @param title The title text to display.
     * @param message The message text to display.
     */
    static void showAlert(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }
}
