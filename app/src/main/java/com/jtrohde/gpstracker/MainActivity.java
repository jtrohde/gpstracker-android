package com.jtrohde.gpstracker;

import com.jtrohde.gpstracker.http.HttpDeleteTask;
import com.jtrohde.gpstracker.http.HttpGetTask;
import com.jtrohde.gpstracker.http.OnHttpResponseListener;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity
        extends ActionBarActivity
        implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener,
        OnHttpResponseListener {

    private ListView itemsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemsListView = (ListView) findViewById(R.id.items);
        itemsListView.setOnItemClickListener(this);
        itemsListView.setOnItemLongClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        HttpGetTask task = new HttpGetTask(this, this);
        task.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.add) {
            // Launch editing activity with no arguments to indicate a new item
            Intent intent = new Intent(this, EditActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.refresh) {
            // Refresh the list
            refresh();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Get selected item
        GpsTrackedItem item = (GpsTrackedItem) parent.getItemAtPosition(position);

        // Launch editing activity for the current item
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("id", item.getId());
        intent.putExtra("name", item.getName());
        intent.putExtra("description", item.getDescription());
        intent.putExtra("latitude", item.getLatLong().getLatitude());
        intent.putExtra("longitude", item.getLatLong().getLongitude());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> parent, View view, final int position, long id) {
        String[] options = { "Delete Item " };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(
                options,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            final int id = ((GpsTrackedItem) parent.getItemAtPosition(position)).getId();
                            String name = ((GpsTrackedItem) parent.getItemAtPosition(position)).getName();
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Confirm Delete")
                                    .setMessage("Delete item '" + name + "'?")
                                    .setPositiveButton(
                                            "Yes",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    HttpDeleteTask task = new HttpDeleteTask(MainActivity.this, id, MainActivity.this);
                                                    task.execute(id);
                                                }
                                            }
                                    )
                                    .setNegativeButton("Cancel", null)
                                    .create()
                                    .show();
                        }
                    }
                }
        );
        builder.create().show();
        return true;
    }

    @Override
    public void onHttpResponse(String requestMethod, int statusCode, String responseText) {
        if ("DELETE".equals(requestMethod)) {
            if (statusCode != 200) {
                Alert.showAlert(this, "", "Failed to retrieve items:\nHTTP status code: " + statusCode + "\nReason: " + responseText);
                return;
            }

            refresh();
        }
        else if ("GET".equals(requestMethod)) {
            if (statusCode != 200) {
                Alert.showAlert(this, "", "Failed to retrieve items:\nHTTP status code: " + statusCode + "\nReason: " + responseText);
                return;
            }

            ArrayList<GpsTrackedItem> items = GpsTrackedItem.jsonToArrayList(responseText);
            if (items == null) {
                Alert.showAlert(this, "", "Failed to parse response text.");
                return;
            }

            // Display items in list
            GpsTrackedItemAdapter adapter = new GpsTrackedItemAdapter(this, items);
            itemsListView.setAdapter(adapter);
        }
    }
}
