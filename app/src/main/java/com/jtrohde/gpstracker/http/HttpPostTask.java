package com.jtrohde.gpstracker.http;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Add a new item.
 *
 * @author Justin Rohde
 */
public class HttpPostTask extends AsyncTask<NameValuePair, Void, HttpResult> {

    /**
     * Will receive notifications upon task completion.
     */
    private OnHttpResponseListener listener;

    /**
     * Progress dialog displayed while performing this task.
     */
    private final ProgressDialog progressDialog;

    public HttpPostTask(Context context, OnHttpResponseListener listener) {
        this.listener = listener;

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Processing Request");
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
    }

    @Override
    protected HttpResult doInBackground(NameValuePair... params) {
        // Create and initialize HTTP client
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://www.jtrohde.com/gpstracker/request.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(new ArrayList<>(Arrays.asList(params))));
        }
        catch (UnsupportedEncodingException e) {
            return null;
        }

        try {
            // Execute request and get response
            HttpResponse httpResponse = client.execute(httpPost);
            if (httpResponse == null) {
                return null;
            }
            else {
                if (httpResponse.getStatusLine() == null) {
                    return null;
                }
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                String responseText = EntityUtils.toString(httpResponse.getEntity());
                return new HttpResult(statusCode, responseText);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(HttpResult httpResult) {
        super.onPostExecute(httpResult);

        if (httpResult != null && listener != null) {
            listener.onHttpResponse("POST", httpResult.getStatusCode(), httpResult.getResponseText());
        }

        progressDialog.dismiss();
    }
}
