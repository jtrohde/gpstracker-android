package com.jtrohde.gpstracker.http;

/**
 * @author Justin Rohde
 */
public class HttpResult {
    /**
     * Status code returned by HTTP response.
     */
    private int statusCode;

    /**
     * Body text returned by HTTP response.
     */
    private String responseText;

    public HttpResult(int statusCode, String responseText) {
        this.statusCode = statusCode;
        this.responseText = responseText;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
