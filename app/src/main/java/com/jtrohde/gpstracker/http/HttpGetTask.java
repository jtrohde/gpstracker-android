package com.jtrohde.gpstracker.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Download existing items.
 *
 * @author Justin Rohde
 */
public class HttpGetTask extends AsyncTask<Void, Void, HttpResult> {

    /**
     * Will receive notifications upon task completion.
     */
    private OnHttpResponseListener listener;

    /**
     * Progress dialog displayed while performing this task.
     */
    private final ProgressDialog progressDialog;

    public HttpGetTask(Context context, OnHttpResponseListener listener) {
        this.listener = listener;

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Processing Request");
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
    }

    @Override
    protected HttpResult doInBackground(Void... params) {
        // Create and initialize HTTP client
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://www.jtrohde.com/gpstracker/request.php/items");
        //HttpGet httpGet = new HttpGet("http://www.google.com");

        try {
            // Execute request and get response
            HttpResponse httpResponse = client.execute(httpGet);
            if (httpResponse == null) {
                return null;
            }
            else {
                if (httpResponse.getStatusLine() == null) {
                    return null;
                }
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                String responseText = EntityUtils.toString(httpResponse.getEntity());
                return new HttpResult(statusCode, responseText);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(HttpResult httpResult) {
        super.onPostExecute(httpResult);

        if (httpResult != null && listener != null) {
            listener.onHttpResponse("GET", httpResult.getStatusCode(), httpResult.getResponseText());
        }

        progressDialog.dismiss();
    }
}
