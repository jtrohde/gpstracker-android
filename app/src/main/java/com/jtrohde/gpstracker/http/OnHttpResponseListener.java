package com.jtrohde.gpstracker.http;

/**
 * Listener for HTTP responses received.
 *
 * @author Justin Rohde
 */
public interface OnHttpResponseListener {
    void onHttpResponse(String requestMethod, int statusCode, String responseText);
}
