package com.jtrohde.gpstracker.http;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

/**
 * Update an existing item with new attributes.
 *
 * @author Justin Rohde
 */
public class HttpPutTask extends AsyncTask<NameValuePair, Void, HttpResult> {

    /**
     * The ID of the item to update.
     */
    private final int id;

    /**
     * Will receive notification upon task completion.
     */
    private OnHttpResponseListener listener;

    /**
     * Progress dialog displayed while performing this task.
     */
    private final ProgressDialog progressDialog;

    public HttpPutTask(Context context, int id, OnHttpResponseListener listener) {
        this.id = id;
        this.listener = listener;

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Processing Request");
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
    }

    @Override
    protected HttpResult doInBackground(NameValuePair... params) {
        // Create and initialize HTTP client
        HttpClient client = new DefaultHttpClient();

        // Build URI for PUT request
        Uri.Builder builder = new Uri.Builder()
                .scheme("http")
                .authority("www.jtrohde.com")
                .appendPath("gpstracker")
                .appendPath("request.php")
                .appendPath("items")
                .appendPath("id")
                .appendPath(String.valueOf(id));

        for (NameValuePair param : params) {
            builder = builder.appendQueryParameter(param.getName(), param.getValue());
        }

        HttpPut httpPut = new HttpPut(builder.build().toString());

        try {
            // Execute request and get response
            HttpResponse httpResponse = client.execute(httpPut);
            if (httpResponse == null) {
                return null;
            }
            else {
                if (httpResponse.getStatusLine() == null) {
                    return null;
                }
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                String responseText = EntityUtils.toString(httpResponse.getEntity());
                return new HttpResult(statusCode, responseText);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(HttpResult httpResult) {
        super.onPostExecute(httpResult);

        if (httpResult != null && listener != null) {
            listener.onHttpResponse("PUT", httpResult.getStatusCode(), httpResult.getResponseText());
        }

        progressDialog.dismiss();
    }
}
