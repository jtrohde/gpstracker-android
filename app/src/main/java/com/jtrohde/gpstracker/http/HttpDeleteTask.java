package com.jtrohde.gpstracker.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Delete the item with the specified ID.
 *
 * @author Justin Rohde
 */
public class HttpDeleteTask extends AsyncTask<Integer, Void, HttpResult> {

    /**
     * The ID of the item to delete.
     */
    private int id;

    /**
     * Will receive the result of the delete operation on task completion.
     */
    private OnHttpResponseListener listener;

    /**
     * Progress dialog displayed while performing this task.
     */
    private final ProgressDialog progressDialog;

    /**
     * Constructor.
     *
     * @param context The context on which the task will run.
     * @param id The ID of the item to delete.
     * @param listener The listener that will receive notification upon task completion.
     */
    public HttpDeleteTask(Context context, int id, OnHttpResponseListener listener) {
        this.id = id;
        this.listener = listener;

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Processing Request");
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
    }

    @Override
    protected HttpResult doInBackground(Integer... params) {
        // Create and initialize HTTP client
        HttpClient client = new DefaultHttpClient();
        HttpDelete httpDelete = new HttpDelete("http://www.jtrohde.com/gpstracker/request.php/items/id/" + id);

        try {
            // Execute request and get response
            HttpResponse httpResponse = client.execute(httpDelete);
            if (httpResponse == null) {
                return null;
            }
            else {
                if (httpResponse.getStatusLine() == null) {
                    return null;
                }
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                String responseText = EntityUtils.toString(httpResponse.getEntity());
                return new HttpResult(statusCode, responseText);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(HttpResult httpResult) {
        super.onPostExecute(httpResult);

        if (httpResult != null && listener != null) {
            listener.onHttpResponse("DELETE", httpResult.getStatusCode(), httpResult.getResponseText());
        }

        progressDialog.dismiss();
    }
}
