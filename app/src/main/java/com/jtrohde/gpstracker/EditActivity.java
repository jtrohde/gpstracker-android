package com.jtrohde.gpstracker;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jtrohde.gpstracker.http.HttpPostTask;
import com.jtrohde.gpstracker.http.HttpPutTask;
import com.jtrohde.gpstracker.http.OnHttpResponseListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author Justin Rohde
 */
public class EditActivity extends FragmentActivity implements OnHttpResponseListener, OnMapReadyCallback {

    /**
     * Displays a map with a marker at the coordinates of the selected object.
     */
    private GoogleMap mMap;

    /**
     * Object name entry.
     */
    private EditText nameEditText;

    /**
     * Object description entry.
     */
    private EditText descriptionEditText;

    /**
     * Latitude.
     */
    private EditText latitudeEditText;

    /**
     * Longitude.
     */
    private EditText longitudeEditText;

    /**
     * ID of the object to update, or 0 if the object is new.
     */
    private int id;

    /**
     * Displayed while obtaining location.
     */
    private ProgressDialog progressDialog;

    /**
     * Updates the map location when longitude or latitude are edited.
     */
    private TextWatcher latLongTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mMap == null) {
                setUpMapIfNeeded();
            }
            if (mMap != null) {
                setUpMap();
            }
        }
    };

    /**
     * Manages location updates.
     */
    private static LocationManager locationManager = null;

    /**
     * Listener for new location updates.
     */
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.i("LocationListener", "Received location from provider: " + location.getProvider());
            Log.i("LocationListener", "Location accuracy: " + location.getAccuracy());

            latitudeEditText.setText(String.valueOf(location.getLatitude()));
            longitudeEditText.setText(String.valueOf(location.getLongitude()));

            locationManager.removeUpdates(locationListener);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.i("LocationListener", "Provider status changed: " + provider + " / " + status);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.i("LocationListener", "Provider enabled: " + provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.i("LocationListener", "Provider disabled: " + provider);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        // Get views
        nameEditText = (EditText) findViewById(R.id.name);
        descriptionEditText = (EditText) findViewById(R.id.description);
        latitudeEditText = (EditText) findViewById(R.id.latitude);
        longitudeEditText = (EditText) findViewById(R.id.longitude);
        Button submitButton = (Button) findViewById(R.id.submit);

        // Get passed values or use defaults
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            id = extras.getInt("id", 0);
            nameEditText.setText(getIntent().getStringExtra("name"));
            descriptionEditText.setText(getIntent().getStringExtra("description"));
            latitudeEditText.setText(String.valueOf(getIntent().getDoubleExtra("latitude", 0)));
            longitudeEditText.setText(String.valueOf(getIntent().getDoubleExtra("longitude", 0)));
            setTitle("Edit Item");
        }
        else {
            nameEditText.setText("New Location");
            descriptionEditText.setText("New Description");
            requestLocationUpdates();
            setTitle("Add Item");
        }

        // Add listeners for lat/long changes
        latitudeEditText.addTextChangedListener(latLongTextWatcher);
        longitudeEditText.addTextChangedListener(latLongTextWatcher);

        // Set submit button text based on state
        submitButton.setText(id == 0 ? "Add" : "Update");
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly installed) and the map has not already been
     * instantiated.. This will ensure that we only ever call {@link #setUpMap()} once when {@link #mMap} is not null. <p> If it isn't installed
     * {@link SupportMapFragment} (and {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to install/update the
     * Google Play services APK on their device. <p> A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not have been completely destroyed during this process
     * (it is likely that it would only be stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this method in
     * {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        }
    }

    public void onSubmitClick(View v) {
        // Validate fields
        String name = nameEditText.getText().toString();
        if (name.trim().length() == 0) {
            Alert.showAlert(this, "", "Name required.");
            return;
        }

        String description = descriptionEditText.getText().toString();
        if (description.trim().length() == 0) {
            Alert.showAlert(this, "", "Description required.");
            return;
        }

        double latitude;
        try {
            latitude = Double.parseDouble(latitudeEditText.getText().toString());
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            Alert.showAlert(this, "", "Latitude invalid.");
            return;
        }

        double longitude;
        try {
            longitude = Double.parseDouble(longitudeEditText.getText().toString());
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            Alert.showAlert(this, "", "Longitude invalid.");
            return;
        }

        // Submit the new item
        ArrayList<NameValuePair> nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("name", name));
        nameValuePairList.add(new BasicNameValuePair("description", description));
        nameValuePairList.add(new BasicNameValuePair("latitude", String.valueOf(latitude)));
        nameValuePairList.add(new BasicNameValuePair("longitude", String.valueOf(longitude)));

        if (id == 0) {
            // Add new item
            HttpPostTask task = new HttpPostTask(this, this);
            NameValuePair[] nameValuePairArray = new NameValuePair[nameValuePairList.size()];
            task.execute(nameValuePairList.toArray(nameValuePairArray));
        }
        else {
            // Update existing item
            HttpPutTask task = new HttpPutTask(this, id, this);
            NameValuePair[] nameValuePairArray = new NameValuePair[nameValuePairList.size()];
            task.execute(nameValuePairList.toArray(nameValuePairArray));
        }
    }

    public void onUseCurrentPositionClick(View v) {
        requestLocationUpdates();
        setUpMap();
    }

    private void setUpMap() {
        String name = nameEditText.getText().toString();

        double latitude, longitude;
        try {
            latitude = Double.parseDouble(latitudeEditText.getText().toString());
        }
        catch (NumberFormatException e) {
            latitude = 0;
        }

        try {
            longitude = Double.parseDouble(longitudeEditText.getText().toString());
        }
        catch (NumberFormatException e) {
            longitude = 0;
        }

        if (mMap != null) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(name));
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12);
            mMap.moveCamera(update);
        }
    }

    private void requestLocationUpdates() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Obtaining Location");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        boolean gpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!gpsProviderEnabled && !networkProviderEnabled) {
            Alert.showAlert(this, "No Location Providers Enabled", "Unable to determine location.");
            return;
        }

        if (gpsProviderEnabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }

        if (networkProviderEnabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }

        Log.i("EditActivity", "Location updates have been requested...");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Check if we were successful in obtaining the map.
        if (mMap != null) {
            setUpMap();
        }
    }

    @Override
    public void onHttpResponse(String requestMethod, int statusCode, String responseText) {
        if (("POST".equals(requestMethod) && statusCode == 201) || ("PUT".equals(requestMethod) && statusCode == 200)) {
            // Successful - exit activity
            finish();
        }
        else {
            Alert.showAlert(this, "", "Error:\n\n" + statusCode + ": " + responseText);
        }
    }
}
